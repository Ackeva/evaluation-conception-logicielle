# -*- coding: utf-8 -*-
"""
Created on Tue May 11 12:25:24 2021

@author: Maëlys
"""
import requests

#données à envoyer en header à la requête    
headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}

#Requête GET pour avoir un id de jeu de cartes
r = requests.get("http://127.0.0.1:8000/creer-cartes/", headers=headers)  #Réalisation de la requête
r_json = r.json() #Récupère le JSON
print(r_json) #Affiche le résultat


#Requête POST pour tirer des cartes
data_json = {"deck_id": str(r_json['deck_id'])} #Données à renvoyer dans le corps de la requête POST
nombre_cartes = 10 #nombre de cartes à tirer

r_post = requests.post("http://127.0.0.1:8000/tirer-cartes/"+str(nombre_cartes), headers=headers, json=data_json) #Réalisation de la requête
post_json = r_post.json() #Récupère le JSON
print(post_json) #Affiche le résultat


#Compter les différentes couleurs
cartes = {'cartes' : post_json['cards']}
r_couleurs = requests.post("http://127.0.0.1:8000/compter-couleurs/", headers=headers, json=cartes) #Réalisation de la requête
print(r_couleurs.json())
