# -*- coding: utf-8 -*-
"""
Created on Wed May 12 12:55:18 2021

@author: Maëlys
"""
from pydantic import BaseModel

class DeckId(BaseModel) :
    deck_id:str
    

class Cartes(BaseModel) :
    cartes:list
    
