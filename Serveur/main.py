# -*- coding: utf-8 -*-
"""
Created on Thu May 13 10:02:38 2021

@author: Maëlys
"""


import requests
from fastapi import FastAPI
import metier

app = FastAPI()


@app.get("/creer-cartes/")
def get_creer_cartes():
    """
    Requête GET qui renvoie l'id d'un paquet de carte aléatoirement
    """
    
    r = requests.get("https://deckofcardsapi.com/api/deck/new/") #Requête sur l'API externe pour tirer un paquet de cartes
 
    r_json = r.json() #Récupération des données au format JSON
 
    deck_id = r_json['deck_id'] #Récupération de l'id
   
    return {"deck_id":deck_id} #Renvoie un dictionnaire avec les informations demandées



@app.post("/tirer-cartes/{nombre_cartes}")
async def post_tirer_cartes(nombre_cartes: int, deck: metier.DeckId): 
    """
    Requête POST qui renvoie un nombre de cartes (selon la demande de l'utilisateur) à partir de l'ID du dernier paquet de cartes utilisées (dans le body de la requête)
    """
    
    requete_lien = "https://deckofcardsapi.com/api/deck/"+str(deck.deck_id)+"/draw/?count="+str(nombre_cartes) #Création du lien de la requête vers l'API externe

    post = requests.get(requete_lien) #Fait la rêquete pour avoir un certain nombre de cartes
    
    p_json = post.json() #Récupération des données au format JSON
    
    return dict({'cards': p_json['cards']}) #Renvoie un dictionnaire avec les informations demandées



@app.post("/compter-couleurs/")
async def post_compter_couleurs(deck: metier.Cartes): 
    #Si les données ne sont pas au bon format, renvoie une erreur
    """
    Requête POST qui renvoie un nombre de cartes (selon la demande de l'utilisateur) à partir de l'ID du dernier paquet de cartes utilisées
    """
    
    #On récupère la liste des couleurs des cartes
    liste_couleurs = []
    for i in range(len(deck.cartes)) :
        liste_couleurs.append(deck.cartes[i]['suit'][0])
        
    #Compter le nombre de chaque élément
    retour = {"H":0,"S":0,"D":0,"C":0}
    
    for i in range(len(liste_couleurs)) :
        if liste_couleurs[i] == "H" :
            retour['H'] += 1
        elif liste_couleurs[i] == "S" :
            retour['S'] += 1
        elif liste_couleurs[i] == "D" :
            retour['D'] += 1
        elif liste_couleurs[i] == "C" :
            retour['C'] += 1
    
    return retour #Renvoie un dictionnaire avec les informations demandées





if __name__ == '__main__':
    app.run(debug=True)
