# -*- coding: utf-8 -*-
"""
Created on Fri May 14 09:48:42 2021

@author: Maëlys
"""

import unittest
import requests

class TestCompterCartes(unittest.TestCase):
    """
     Classe de test permettant de valider le bon fonctionnement de compter_cartes
    """

    def test_compter_cartes(self):
        """
        @pre: -
        @post: a verifie le fonctionnement correct de la fonction compter_cartes
        """
        
        #données à envoyer en header à la requête    
        headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}

        
        cartes = {'cartes': [{'code': 'AS',
                           'image': 'https://deckofcardsapi.com/static/img/AS.png',
                           'images': {'svg': 'https://deckofcardsapi.com/static/img/AS.svg',
                            'png': 'https://deckofcardsapi.com/static/img/AS.png'},
                           'value': 'ACE',
                           'suit': 'SPADES'},
                          {'code': '2S',
                           'image': 'https://deckofcardsapi.com/static/img/2S.png',
                           'images': {'svg': 'https://deckofcardsapi.com/static/img/2S.svg',
                            'png': 'https://deckofcardsapi.com/static/img/2S.png'},
                           'value': '2',
                           'suit': 'SPADES'}]}
        nombre_cartes = len(cartes['cartes'])        
        
        r_couleurs = requests.post("http://127.0.0.1:8000/compter-couleurs/", headers=headers, json=cartes) #Réalisation de la requête
        dict_couleurs = r_couleurs.json()
        
        print(dict_couleurs)
        
        self.assertEqual(True, 'H' in dict_couleurs)
        self.assertEqual(True, 'S' in dict_couleurs)
        self.assertEqual(True, 'D' in dict_couleurs)
        self.assertEqual(True, 'C' in dict_couleurs)
        self.assertEqual(nombre_cartes, dict_couleurs['H'] + dict_couleurs['S'] + dict_couleurs['D'] + dict_couleurs['C'])


if __name__ == '__main__':
    unittest.main()
